# Gitlab runner on Windows

Howto

1. Create a new server. Install Server 2016.
2. [Install runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/windows.md)
3. [Register](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/register/index.md).
   Use "shell" as the executor, because nothing else works on Windows yet.
4. Install choco
5. `choco install git`
6. Add `shell = "powershell"` to the `[[runners]]` section of `config.toml`,
   because apparently [#1052](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1052)
   is still a thing.

# Dependencies

Docker won't be available, so you'll need to install the necessary dependencies
on the box yourself. Be sensible here, you'll want to isolate this as much as
possible, so make sure your bundle scripts use virtualenv, bundler, or some
other suitable tool.

Don't just install crap willy nilly, or you will break other peoples' builds.

# Why isn't this automated yet?

Same reason the Linux GitLab Runner isn't automated yet.

*See [AWS_Foundation/GitLabRunnerOnSpot](https://git.ptwo.kineticit.com.au/AWS_Foundation/GitLabRunnerOnSpot)*

